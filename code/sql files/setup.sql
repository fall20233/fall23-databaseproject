--creating the customer related tables
CREATE TABLE province (
    province_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    province    VARCHAR2(20),
    country     VARCHAR2(25)
);

CREATE TABLE Location (
    city VARCHAR2(25)   PRIMARY KEY,
    province_id         NUMBER REFERENCES province(province_id)
);

CREATE TABLE Customers (
    customer_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
    fname       VARCHAR2(30)    NOT NULL,
    lname       VARCHAR2(30)    NOT NULL,
    email       VARCHAR2(50)    NOT NULL
);

CREATE TABLE address (
    customer_id NUMBER REFERENCES Customers(customer_id),
    address     VARCHAR2(50),
    city        VARCHAR2(25)    REFERENCES Location(city)
);

--creating products tables
create table products(
    product_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
    name varchar2(30) NOT NULL UNIQUE,
    category VARCHAR2(30) NOT NULL,
    price NUMBER(10,2) NOT NULL
);

CREATE TABLE stores(
    store_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
    name VARCHAR2(30) NOT NULL UNIQUE
);

CREATE TABLE reviews(
    review_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    score NUMBER(2) CHECK(score > -1 AND score < 6),
    flag NUMBER(1) NOT NULL,
    DESCRIPTION VARCHAR2(600)
);

CREATE TABLE Order_info(
    order_id NUMBER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
    order_date DATE,
    quantity NUMBER(3)
);

CREATE TABLE warehouse_info (
    name                VARCHAR2(30)    primary key ,
    warehouse_address   VARCHAR2(30)    NOT NULL,
    city                VARCHAR2(25)    REFERENCES Location(city)
);

CREATE TABLE warehouse(
    name        VARCHAR2(30)    REFERENCES warehouse_info(name),
    product_id  number(4)       REFERENCES products(product_id),
    quantity    number(7)
);

CREATE TABLE orders (
    order_id    NUMBER  NOT NULL    REFERENCES order_info(order_id),
    customer_id NUMBER  NOT NULL    REFERENCES Customers(customer_id),
    product_id  NUMBER  NOT NULL    REFERENCES products(product_id),
    store_id    NUMBER  NOT NULL    REFERENCES stores(store_id),
    review_id   NUMBER  REFERENCES reviews(review_id)
);

CREATE TABLE log_table (
    log_id              NUMBER GENERATED BY DEFAULT AS IDENTITY,
    user_change         VARCHAR2(30)    NOT NULL,
    table_name          VARCHAR(20)     NOT NULL,
    action              VARCHAR2(20)    NOT NULL,
    log_date            DATE            NOT NULL
);

--Triggers:
--Deleting a review if that review is flagged more than 2 times
CREATE OR REPLACE TRIGGER flag_check
    AFTER UPDATE ON reviews
DECLARE
    CURSOR review_cursor IS
        SELECT review_id 
        FROM reviews 
        WHERE flag > 2;
BEGIN
    FOR cur IN review_cursor LOOP
        UPDATE orders
        SET review_id = null
        WHERE review_id = cur.review_id;
    END LOOP;

    DELETE FROM reviews 
    WHERE flag > 2;
END;
/

--Logs inserts, updates and deletions on customers table
CREATE OR REPLACE TRIGGER customer_log
    AFTER INSERT OR UPDATE OR DELETE 
    ON Customers
    FOR EACH ROW
DECLARE
    log_action log_table.action%TYPE;
BEGIN
    log_action := CASE
        WHEN INSERTING THEN 'INSERT'
        WHEN UPDATING  THEN 'UPDATE'
        WHEN DELETING  THEN 'DELETE'
    END;
    
    INSERT INTO log_table(user_change, table_name, action, log_date)
    VALUES (USER, 'Customers', log_action, SYSDATE);
END;
/

--Logs insert, updates and deletions on products table
CREATE OR REPLACE TRIGGER product_log
    AFTER INSERT OR UPDATE OR DELETE
    ON products
    FOR EACH ROW
DECLARE
    log_action log_table.action%TYPE;
BEGIN
    log_action := CASE
        WHEN INSERTING THEN 'INSERT'
        WHEN UPDATING  THEN 'UPDATE'
        WHEN DELETING  THEN 'DELETE'
    END;
    
    INSERT INTO log_table(user_change, table_name, action, log_date)
    VALUES (USER, 'Products', log_action, SYSDATE);
END;
/
