-- Assumptions:
-- tomato is 5$
-- train X745 is 1 000 000$
-- customers can only have 1 email registered

--provinces
INSERT INTO province (province,country)
values('Ontario','canada');
INSERT INTO province (province,country)
values('Quebec','canada');
INSERT INTO province (province,country)
values('Alberta','canada');

--location
INSERT INTO LOCATION
VALUES ('Montreal',2);
INSERT INTO LOCATION
VALUES ('Laval',2);
INSERT INTO LOCATION
VALUES ('Brossard',2);
INSERT INTO LOCATION
VALUES ('Toronto',1);
INSERT INTO LOCATION
VALUES ('Calgary',3);
INSERT INTO LOCATION
VALUES ('Saint-laurent',2);
INSERT INTO LOCATION
VALUES ('Villera Saint-Michel',2);
INSERT INTO LOCATION
VALUES ('Ottawa',1);
INSERT INTO LOCATION
VALUES ('Quebec City',2);


--Customers
INSERT INTO Customers (fname,lname,email)
VALUES ('Mahsa','Sadeghi','msadeghi@dawsoncollege.qc.ca');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Alex','Brown','alex@gmail.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Martin','Alexandre','marting@yahoo.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Daneil','Hanne','daneil@yahoo.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('John','Boura','bdoura@gmail.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Ari','Brown','b.a@gmail.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Amanda','Harry','am.harry@yahioo.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Jack','Jonhson','johnson.a@gmail.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('John','Belle','abcd@yahoo.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Martin','Li','m.li@gmail.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Noah','Garcia','g.noah@yahoo.com');
INSERT INTO Customers (FNAME,LNAME,email)
VALUES ('Olivia','Smith','smith@hotmail.com');

--address
INSERT INTO address
VALUES(1,'Dawson college','Montreal');
INSERT INTO address
VALUES(1,'104 gill street','Toronto');
INSERT INTO address
VALUES(2,'090 boul Saint Laurent','Montreal');
INSERT INTO address
VALUES(3,NULL,'Brossard');
INSERT INTO address
VALUES(4,'100 atwater street','Toronto');
INSERT INTO address
VALUES(5,'100 young street','Toronto');
INSERT INTO address
VALUES(6,NULL,NULL);
INSERT INTO address
VALUES(7,'100 boul Saint Laurent','Montreal');
INSERT INTO address
VALUES(8,NULL,'Calgary');
INSERT INTO address
VALUES(9,'105 young street','Toronto');
INSERT INTO address
VALUES(10,'87 Boul Saint Laurent','Montreal');
INSERT INTO address
VALUES(11,'22222 Happy Street','Laval');
INSERT INTO address
VALUES(12,'76 Bould decalthon','Laval');


--products
INSERT INTO Products (name,category,price)
VALUES ('laptop ASUS 104S','electronics',970);
INSERT INTO Products (name,category,price)
VALUES ('apple','grocery',5);
INSERT INTO Products (name,category,price)
VALUES ('SIMS CD','Video Games',16.67);
INSERT INTO Products (name,category,price)
VALUES ('orange','grocery',2);
INSERT INTO Products (name,category,price)
VALUES ('Barbie Movie','DVD',30);
INSERT INTO Products (name,category,price)
VALUES ('L''Oreal Normal Hair','Health',10);
INSERT INTO Products (name,category,price)
VALUES ('BMW iX Lego','lego',40);
INSERT INTO Products (name,category,price)
VALUES ('BMMW i6','Cars',50000);
INSERT INTO Products (name,category,price)
VALUES ('Truck 500c','Vehicle',856600);
INSERT INTO Products (name,category,price)
VALUES ('paper towel','beauty',16.67);
INSERT INTO Products (name,category,price)
VALUES ('plum','grocery',1.67);
INSERT INTO Products (name,category,price)
VALUES ('Lamborghini lego','Toys',40);
INSERT INTO Products (name,category,price)
VALUES ('chicken','grocery',9.50);
INSERT INTO Products (name,category,price)
VALUES ('pasta','grocery',4.50);
INSERT INTO Products (name,category,price)
VALUES ('PS5','electronics',200);
INSERT INTO Products (name,category,price)
VALUES ('tomato','grocery',5);
INSERT INTO Products (name,category,price)
VALUES ('Train X745','Vehicle',1000000);


--stores
INSERT INTO stores(name)
VALUES ('marche adonis');
INSERT INTO stores(name)
VALUES ('marche atwater');
INSERT INTO stores(name)
VALUES ('dawson store');
INSERT INTO stores(name)
VALUES ('store magic');
INSERT INTO stores(name)
VALUES ('movie store');
INSERT INTO stores(name)
VALUES ('super rue champlain'); 
INSERT INTO stores(name)
VALUES ('toy r  us');
INSERT INTO stores(name)
VALUES ('Dealer one');
INSERT INTO stores(name)
VALUES ('dealer montreal');
INSERT INTO stores(name)
VALUES ('movie start');
INSERT INTO stores(name)
VALUES ('star store');


--reviews
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,'it was affordable.');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(3,0,'quality was not good');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(2,1,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(5,0,'highly recommend');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,'did not worth the price');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,'missing some parts');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(5,1,'trash');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(2,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(5,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(3,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,'missing some parts');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,'great product');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(5,1,'bad quality');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(5,0,null);
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(1,2,'worse car i have droven!');
INSERT INTO REVIEWS(score,flag,DESCRIPTION)
VALUES(4,0,null);


--order_info
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-04-21','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-23','YYYY-MM-DD'),2);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-01','YYYY-MM-DD'),3);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-23','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-23','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-10','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-11','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-10','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(null,1);
INSERT INTO Order_info(order_date,quantity)
VALUES(null,3);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2020-05-06','YYYY-MM-DD'),6);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2019-09-12','YYYY-MM-DD'),3);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2010-10-11','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2022-05-06','YYYY-MM-DD'),7);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-07','YYYY-MM-DD'),2);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-08-10','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-23','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2023-10-02','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2019-04-03','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2021-12-19','YYYY-MM-DD'),3);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2020-01-20','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2022-10-11','YYYY-MM-DD'),1);
INSERT INTO Order_info(order_date,quantity)
VALUES(TO_DATE('2021-12-29','YYYY-MM-DD'),3);

--warehouse-info

INSERT INTO warehouse_info
VALUES('warehouse A','100 rue William', 'Saint-laurent');
INSERT INTO warehouse_info
VALUES('warehouse B','304 Rue François-Perrault', 'Villera Saint-Michel');
INSERT INTO warehouse_info
VALUES('warehouse C','86700 Weston Rd', 'Toronto');
INSERT INTO warehouse_info
VALUES('warehouse D','170  Sideroad', 'Quebec City');
INSERT INTO warehouse_info
VALUES('warehouse E','1231 Trudea road', 'Ottawa');
INSERT INTO warehouse_info
VALUES('warehouse F','16  Whitlock Rd', 'Calgary');

--warehouse
INSERT INTO warehouse
VALUES('warehouse A',1,1000);
INSERT INTO warehouse
VALUES('warehouse B',2,24980);
INSERT INTO warehouse
VALUES('warehouse C',3,103);
INSERT INTO warehouse
VALUES('warehouse D',4,35405);
INSERT INTO warehouse
VALUES('warehouse E',5,40);
INSERT INTO warehouse
VALUES('warehouse F',6,450);
INSERT INTO warehouse
VALUES('warehouse A',7,10);
INSERT INTO warehouse
VALUES('warehouse A',8,6);
INSERT INTO warehouse
VALUES('warehouse E',9,1000);
INSERT INTO warehouse
VALUES('warehouse F',10,3532);
INSERT INTO warehouse
VALUES('warehouse C',11,43242);
INSERT INTO warehouse
VALUES('warehouse B',9,39484);
INSERT INTO warehouse
VALUES('warehouse D',11,6579);
INSERT INTO warehouse
VALUES('warehouse E',12,98765);
INSERT INTO warehouse
VALUES('warehouse E',13,43523);
INSERT INTO warehouse
VALUES('warehouse A',14,2132);
INSERT INTO warehouse
VALUES('warehouse D',15,123);
INSERT INTO warehouse
VALUES('warehouse A',16,352222);
INSERT INTO warehouse
VALUES('warehouse E',17,4543);


--orders
INSERT INTO ORDERS
VALUES(1,1,1,1,1);
INSERT INTO ORDERS
VALUES(2,2,2,2,2);
INSERT INTO ORDERS
VALUES(3,3,3,3,3);
INSERT INTO ORDERS
VALUES(4,4,4,4,4);
INSERT INTO ORDERS
VALUES(5,2,5,5,5);
INSERT INTO ORDERS
VALUES(6,3,6,6,6);
INSERT INTO ORDERS
VALUES(7,1,7,7,7);
INSERT INTO ORDERS
VALUES(8,5,8,8,8);
INSERT INTO ORDERS
VALUES(9,6,9,9,9);
INSERT INTO ORDERS
VALUES(10,7,10,10,10);
INSERT INTO ORDERS
VALUES(11,8,11,2,11);
INSERT INTO ORDERS
VALUES(12,3,6,6,12);
INSERT INTO ORDERS
VALUES(13,1,12,7,13);
INSERT INTO ORDERS
VALUES(14,1,11,2,14);
INSERT INTO ORDERS
VALUES(15,1,12,7,15);
INSERT INTO ORDERS
VALUES(16,9,8,8,16);
INSERT INTO ORDERS
VALUES(17,2,3,10,17);
INSERT INTO ORDERS
VALUES(18,2,5,7,18);
INSERT INTO ORDERS
VALUES(19,10,13,1,19);
INSERT INTO ORDERS
VALUES(20,12,14,2,20);
INSERT INTO ORDERS
VALUES(21,11,15,11,null);
INSERT INTO ORDERS
VALUES(22,1,7,7,21);
INSERT INTO ORDERS
VALUES(23,12,14,10,22);