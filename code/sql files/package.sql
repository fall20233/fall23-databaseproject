DROP PACKAGE orders_utilities;

-- package used to minimze code and manage database more effectively from orcale and the JDBC
CREATE PACKAGE orders_utilities AS

    PROCEDURE addCustomer(new_customer IN customers_type, new_address IN VARCHAR2, new_city IN VARCHAR2);
    PROCEDURE updateCustomerInfo(existing_id IN NUMBER, first_name IN VARCHAR2, last_name IN VARCHAR2, e_mail IN VARCHAR2);
    
    PROCEDURE addProduct(new_product IN products_type, warehouse_name IN VARCHAR2, prod_quantity IN NUMBER);
    PROCEDURE updateProductInfo(existing_id IN NUMBER, newName IN VARCHAR2, newCategory IN VARCHAR2, newPrice IN NUMBER);
    PROCEDURE removeProduct(toDelete_id IN NUMBER);
    
    PROCEDURE addOrder(amount_bought IN NUMBER, customer IN NUMBER, product IN NUMBER, store IN NUMBER);
    PROCEDURE cancelOrder(toDeleteOrder_id IN NUMBER);
    
    PROCEDURE addReview(existing_order IN orders_type, new_score IN NUMBER, new_descrip IN VARCHAR2);
    PROCEDURE updateReview(update_id IN NUMBER, new_score IN NUMBER, new_desc IN VARCHAR2, new_flag IN NUMBER);
    PROCEDURE removeReview(delete_id IN NUMBER);
    
    PROCEDURE updateWarehouseQuantity(product_bought IN NUMBER, amount IN NUMBER, OrderMade IN NUMBER);
    procedure deleteWarehouse(warehouseName in VARCHAR2);
    
    FUNCTION getAllProducts RETURN SYS_REFCURSOR;
    FUNCTION getProductsByCategory(searchCategory IN VARCHAR2) RETURN SYS_REFCURSOR;
    FUNCTION getWarehouseProductQuantities RETURN SYS_REFCURSOR;
    
    FUNCTION getAllReviews RETURN SYS_REFCURSOR;
    FUNCTION getAVGReviewsFor(product IN NUMBER) RETURN NUMBER;
    FUNCTION getFlaggedReviews RETURN SYS_REFCURSOR;
    
    FUNCTION getAllOrders RETURN SYS_REFCURSOR;
    FUNCTION getOrdersFromStore(anyStore IN NUMBER) RETURN SYS_REFCURSOR;
    FUNCTION getOrdersFromCustomer(customer IN NUMBER) RETURN SYS_REFCURSOR;
    FUNCTION getOrdersMadeOn(dateMade IN DATE) RETURN SYS_REFCURSOR;
    FUNCTION getOrder(orderId in NUMBER) RETURN SYS_REFCURSOR;
    
    FUNCTION getAllLogs RETURN SYS_REFCURSOR;
    
    FUNCTION getCategories RETURN SYS_REFCURSOR;
    FUNCTION getStoreIds RETURN SYS_REFCURSOR;

    is_unique EXCEPTION;
    is_inexistent EXCEPTION;
    not_enough_stock EXCEPTION;
    
END orders_utilities;
/

CREATE PACKAGE BODY orders_utilities AS

    --Adds a new customer with their address
       PROCEDURE addCustomer(new_customer IN customers_type, new_address IN VARCHAR2, new_city IN VARCHAR2) AS
       BEGIN
            INSERT INTO Customers(customer_id, fname, lname, email)
            VALUES(new_customer.customer_id, new_customer.fname, new_customer.lname, new_customer.email);
        
            INSERT INTO address(customer_id, address, city)
            VALUES(new_customer.customer_id, new_address, new_city);
       END;
       
       
       --updates information of selected customer
       PROCEDURE updateCustomerInfo(existing_id IN NUMBER, first_name IN VARCHAR2, last_name IN VARCHAR2, e_mail IN VARCHAR2) AS
       BEGIN
            UPDATE Customers 
            SET     fname = first_name,
                    lname = last_name,
                    email = e_mail
            WHERE customer_id = existing_id;
       END; 
       
       --Adds a new product with their warehouse
       PROCEDURE addProduct(new_product IN products_type, warehouse_name IN VARCHAR2, prod_quantity IN NUMBER) AS
        name_count NUMBER;
        new_id NUMBER;
       BEGIN
            --Getting results from our selected name
            SELECT COUNT(*) INTO name_count
            FROM products
            WHERE name = new_product.name;
        
            --check if selected name already exists
            IF name_count > 0 THEN
                RAISE is_unique;
            ELSE
                INSERT INTO products(name, category, price)
                VALUES(new_product.name, new_product.category,new_product.price);
                
                SELECT MAX(product_id) INTO new_id
                FROM products;
                
                INSERT INTO warehouse(name, product_id, quantity)
                VALUES(warehouse_name, new_id, prod_quantity);
            END IF;
       END;
       
       --updates information of selected product
       PROCEDURE updateProductInfo(existing_id IN NUMBER, newName IN VARCHAR2, newCategory IN VARCHAR2, newPrice IN NUMBER) AS
            name_count NUMBER;
       BEGIN
             SELECT COUNT(*) INTO name_count
            FROM products
            WHERE name = newName AND product_id != existing_id;
        
            --check if selected name already exists
            IF name_count > 0 THEN
                RAISE is_unique;
            ELSE
                UPDATE Products
                SET     name = newName,
                        category = newCategory,
                        price = newPrice
                WHERE product_id = existing_id;
            END IF;
       END;
       
       --removes a product entirely from products table and all activites in orders, order_info and warehouse tables
       PROCEDURE removeProduct(toDelete_id IN NUMBER) AS
            CURSOR cur IS
                SELECT order_id
                FROM orders
                WHERE product_id = toDelete_id;
       BEGIN
            FOR i IN cur LOOP
                DELETE FROM orders
                WHERE order_id = i.order_id;
            
                DELETE FROM order_info
                WHERE order_id = i.order_id;  
            END LOOP;
            DELETE FROM warehouse
            WHERE product_id = toDelete_id;
       
            DELETE FROM products
            WHERE product_id = toDelete_id;
       END;
       
       --helper method that updates the inventory in warehouse when an order is made or canceled
       PROCEDURE updateWarehouseQuantity(product_bought IN NUMBER, amount IN NUMBER, OrderMade IN NUMBER) AS
       BEGIN
            IF OrderMade > 0 THEN
                UPDATE warehouse SET quantity = quantity - amount
                WHERE product_id = product_bought;
            ELSE
                UPDATE warehouse SET quantity = quantity + amount
                WHERE product_id = product_bought;
            END IF;
       END;

       --removes a warehouse and it's info
       PROCEDURE deleteWarehouse(warehouseName IN VARCHAR2) AS
            count_ware NUMBER;
       BEGIN
            SELECT COUNT(*) INTO count_ware
            FROM WAREHOUSE_INFO
            WHERE name = warehouseName;

            IF count_ware = 0 THEN
                RAISE is_inexistent;
            ELSE
                DELETE FROM warehouse WHERE name = warehouseName;
                DELETE FROM WAREHOUSE_INFO WHERE name = warehouseName;
            END IF;
       END;
       
       --adds an order and updates the inventory in the product's warehouse
       PROCEDURE addOrder(amount_bought IN NUMBER, customer IN NUMBER, product IN NUMBER, store IN NUMBER) AS
            new_id              NUMBER;
            amount_available    NUMBER;
        BEGIN
            SELECT quantity INTO amount_available
            FROM warehouse
            WHERE product_id = product;
            
            IF amount_bought > amount_available THEN
                RAISE not_enough_stock;
            ELSE
                INSERT INTO order_info(order_date, quantity)
                VALUES(SYSDATE,amount_bought);
                
                SELECT MAX(order_id) INTO new_id
                FROM order_info;
    
                INSERT INTO ORDERS(order_id, customer_id, product_id, store_id, review_id)
                VALUES(new_id, customer, product, store, null);
                
                updateWarehouseQuantity(product, amount_bought, 1);
            END IF;
        END;
       
       --removes a record from the orders table and order_info table and updates warehouse quantity for canceled purchase
       PROCEDURE cancelOrder(toDeleteOrder_id IN NUMBER) AS
            count_order NUMBER;
            toDeleteProduct_id NUMBER;
            amount NUMBER;
       BEGIN
            SELECT COUNT(*) INTO count_order
            FROM order_info
            WHERE order_id = toDeleteOrder_id;

            SELECT product_id INTO toDeleteProduct_id
            FROM ORDERS
            WHERE order_id = toDeleteOrder_id;

            SELECT QUANTITY INTO amount
            FROM order_info
            WHERE order_id = toDeleteOrder_id;
                
            IF count_order = 0 THEN
                RAISE is_inexistent;
            ELSE
                DELETE FROM Orders
                WHERE order_id = toDeleteOrder_id;
       
                DELETE FROM Order_info
                WHERE order_id = toDeleteOrder_id;
                
                updateWarehouseQuantity(toDeleteProduct_id, amount, 0);
            END IF;
       END;
       
       --adds a review to an existing order
       PROCEDURE addReview(existing_order IN orders_type, new_score IN NUMBER, new_descrip IN VARCHAR2) AS
            new_id      NUMBER;
            count_order NUMBER;
       BEGIN
            SELECT COUNT(*) INTO count_order
            FROM orders
            WHERE order_id = existing_order.order_id AND 
                customer_id = existing_order.customer_id AND 
                product_id = existing_order.product_id;
                
            IF count_order = 0 THEN
                RAISE is_inexistent;
            ELSE
       
                INSERT INTO reviews(score, flag, description)
                VALUES(new_score, 0, new_descrip);
            
                SELECT MAX(review_id) INTO new_id
                FROM reviews;
            
                UPDATE orders
                SET review_id = new_id
                WHERE order_id = existing_order.order_id;
            END IF;
       END; 
       
       --updates an existing review from the reviews table
       PROCEDURE updateReview(update_id IN NUMBER, new_score IN NUMBER, new_desc IN VARCHAR2,new_flag IN NUMBER) AS
            review_count NUMBER;
       BEGIN
            SELECT COUNT(*) INTO review_count
            FROM reviews
            WHERE review_id = update_id;
            
            IF review_count = 0 THEN
                RAISE is_inexistent;
            ELSE
                UPDATE reviews
                SET     score = new_score,
                        description = new_desc
                WHERE review_id = update_id;
            END IF;
       END;
       
       --removes a review from the reviews table and sets the orders record's review_id field to null
       PROCEDURE removeReview(delete_id IN NUMBER) AS
       review_count     NUMBER;
       affected_order   NUMBER;
       BEGIN
            SELECT COUNT(*) INTO review_count
            FROM reviews
            WHERE review_id = delete_id;
            
            IF review_count = 0 THEN
                RAISE is_inexistent;
            ELSE
                SELECT order_id INTO affected_order
                FROM orders
                WHERE review_id = delete_id;
                
                UPDATE orders
                SET review_id = null
                WHERE order_id = affected_order;
            
                DELETE FROM reviews
                WHERE review_id = delete_id;
            END IF;
       END;
       
       --- functions ---
       
       --gets all products from the product table
       FUNCTION getAllProducts RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR   
                SELECT p.PRODUCT_ID AS "product_id",
                p.name AS "name",
                p.category AS "category",
                p.price AS "price"
                FROM PRODUCTS p
                ORDER BY p.product_id;
            RETURN cur;
        END;
    
        --gets all the product from a selected category
        FUNCTION getProductsByCategory(searchCategory IN VARCHAR2) RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR   
                SELECT p.PRODUCT_ID AS "product_id",
                p.name AS "name",
                p.category AS "category",
                p.price AS "price"
                FROM PRODUCTS p
                WHERE p.CATEGORY = searchCategory
                ORDER BY p.product_id;
            RETURN cur;
        END;
    
        --displays the quantity for each product in their warehouses
        FUNCTION getWarehouseProductQuantities RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT w.name AS "wName", 
                p.name AS "pName",
                w.quantity as "pQty"
                FROM WAREHOUSE w
                inner join PRODUCTS P using(PRODUCT_ID)
                ORDER BY w.name;
            RETURN cur;
        END;
        
        --gets all reviews from the reviews table
        FUNCTION getAllReviews RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT review_id AS "reviewId",
                score AS "score",
                flag AS "flag",
                description AS "description"
                FROM reviews
                ORDER BY review_id;
            RETURN cur;
        END;
        
        --gets the average review score for a specific product
        FUNCTION getAVGReviewsFor(product IN NUMBER) RETURN NUMBER IS
            count_orders    NUMBER;
            avgScore        NUMBER;
        BEGIN
            SELECT COUNT(product_id) INTO count_orders
            FROM orders
            WHERE product_id = product;
            
            IF count_orders = 0 THEN
                RAISE is_inexistent;
            ELSE
                SELECT AVG(score) INTO avgScore
                FROM reviews r INNER JOIN orders o ON r.review_id = o.review_id
                INNER JOIN products p ON o.product_id = p.product_id
                WHERE p.product_id = product;
                
                RETURN avgScore;
            END IF;
        END;

        --gets all reviews that have been flagged
        FUNCTION getFlaggedReviews RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT review_id AS "ReviewID",
                score AS "score",
                flag AS "flag",
                description AS "description"
                FROM REVIEWS
                WHERE flag > 0;
            RETURN cur;
        END;

        --gets a specific order from orders table
        FUNCTION getOrder(orderId in NUMBER) RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT order_id AS "OrderId",
                CUSTOMER_ID AS "CustomerId",
                PRODUCT_ID AS "ProductId",
                STORE_ID AS "StoreId",
                REVIEW_ID AS "ReviewId"
                FROM ORDERS
                where ORDER_ID = orderId;
            return cur;
        END;

        --gets all orders from the orders table
        FUNCTION getAllOrders RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT o.order_id AS "OrderId",
                c.fname AS "FirstName",
                c.lname AS "LastName",
                p.name AS "ProductName",
                s.name AS "StoreName",
                o.review_id AS "ReviewId"
                FROM orders o INNER JOIN customers c ON o.customer_id = c.customer_id
                INNER JOIN products p ON o.product_id = p.product_id
                INNER JOIN stores s ON o.store_id = s.store_id
                ORDER BY o.order_id;
            RETURN cur;
        END;

        --gets all store-ids from stores table
        FUNCTION getStoreIds RETURN SYS_REFCURSOR IS
            cur         SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT s.name AS "storeName",
                s.store_id AS "storeId"
                FROM stores s
                ORDER BY S.STORE_ID;
            RETURN cur;
        END;
        
        --gets all orders from a specific store
        FUNCTION getOrdersFromStore(anyStore IN NUMBER) RETURN SYS_REFCURSOR IS
            cur         SYS_REFCURSOR;
            storeCount NUMBER;
        BEGIN
            SELECT COUNT(order_id) INTO storeCount
            FROM orders
            WHERE store_id = anyStore;
            
            IF storeCount = 0 THEN
                RAISE is_inexistent;
            ELSE
                OPEN cur FOR
                    SELECT o.order_id AS "OrderId",
                    c.fname AS "FirstName",
                    c.lname AS "LastName",
                    p.name AS "ProductName",
                    s.name AS "StoreName",
                    o.review_id AS "ReviewId"
                    FROM orders o INNER JOIN customers c ON o.customer_id = c.customer_id
                    INNER JOIN products p ON o.product_id = p.product_id
                    INNER JOIN stores s ON o.store_id = s.store_id
                    WHERE s.store_id = anyStore
                    ORDER BY o.order_id;
                RETURN cur;
            END IF;
        END;
        
        --gets all orders made a specific customer
        FUNCTION getOrdersFromCustomer(customer IN NUMBER) RETURN SYS_REFCURSOR IS
            cur             SYS_REFCURSOR;
            customerCount   NUMBER;
        BEGIN
            SELECT COUNT(order_id) INTO customerCount
            FROM orders
            WHERE customer_id = customer;
            
            IF customerCount = 0 THEN
                RAISE is_inexistent;
            ELSE
                OPEN cur FOR
                    SELECT o.order_id AS "OrderId",
                    c.fname AS "FirstName",
                    c.lname AS "LastName",
                    p.name AS "ProductName",
                    s.name AS "StoreName",
                    o.review_id AS "ReviewId"
                    FROM orders o INNER JOIN customers c ON o.customer_id = c.customer_id
                    INNER JOIN products p ON o.product_id = p.product_id
                    INNER JOIN stores s ON o.store_id = s.store_id
                    WHERE c.customer_id = customer
                    ORDER BY o.order_id;
                RETURN cur;
            END IF;
        END;
        
        --gets orders made on a certain date
        FUNCTION getOrdersMadeOn(dateMade IN DATE) RETURN SYS_REFCURSOR IS
            cur         SYS_REFCURSOR;
            orderCount  NUMBER;
        BEGIN
            SELECT COUNT(o.order_id) INTO orderCount
            FROM orders o INNER JOIN order_info f
            ON o.order_id = f.order_id
            WHERE f.order_date = dateMade;
            
            IF orderCount = 0 THEN
                RAISE is_inexistent;
            ELSE
                OPEN cur FOR
                    SELECT o.order_id AS "OrderId",
                    c.fname AS "FirstName",
                    c.lname AS "LastName",
                    p.name AS "ProductName",
                    s.name AS "StoreName",
                    o.review_id AS "ReviewId"
                    FROM orders o INNER JOIN customers c ON o.customer_id = c.customer_id
                    INNER JOIN products p ON o.product_id = p.product_id
                    INNER JOIN stores s ON o.store_id = s.store_id
                    INNER JOIN order_info f ON o.order_id = f.order_id
                    WHERE f.order_date = dateMade
                    ORDER BY o.order_id;
                RETURN cur;
            END IF;
        END;
        
        --function to display all logs made from the log_table
        FUNCTION getAllLogs RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT log_id AS "LogId",
                user_change AS "UserChange",
                table_name AS "TableName",
                action AS "Action",
                log_date AS "LogDate"
                FROM log_table
                ORDER BY log_id;
            RETURN cur;
        END;

        --gets all categories from the products table
        FUNCTION getCategories RETURN SYS_REFCURSOR IS
            cur SYS_REFCURSOR;
        BEGIN
            OPEN cur FOR
                SELECT UNIQUE category AS "category" FROM products;
            RETURN cur;
        END;
    
END orders_utilities;
/ --this slash sometimes causes an error when running the file but removing it causes an error so we're just going to leave it


        ---- Testing the package procedures ----


--addCustomer test
DECLARE
    new_custom customers_type;
    custom_id NUMBER;
BEGIN
    SELECT MAX(customer_id) INTO custom_id
    FROM customers;
    
    custom_id := custom_id + 1;
    new_custom := customers_type(custom_id, 'Alec', 'Morin', 'thisis@myemail');
    orders_utilities.addCustomer(new_custom, 'thisismyaddress', 'Montreal');
END;
/

--updateCustomerInfo test
DECLARE
    existing_id NUMBER(2);
    first_name  VARCHAR2(20);
    last_name   VARCHAR2(20);
    e_mail      VARCHAR2(30);
BEGIN
    existing_id := 2;
    first_name  := 'NewAlec';
    last_name   := 'Morin';
    e_mail      := 'hello@email';
    orders_utilities.updateCustomerInfo(existing_id, first_name, last_name, e_mail);
END;

--addProduct test
DECLARE
    new_product products_type;
BEGIN
    new_product := products_type(90, 'AlecInator 2000', 'electronics', 1999);
    orders_utilities.addProduct(new_product, 'warehouse A', 4);
    
    EXCEPTION
        WHEN orders_utilities.is_unique THEN
        DBMS_OUTPUT.PUT_LINE('Cannot insert a new product with the same name. The name field is unique');
        WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Something went wrong'); 
END;
/

--updateProductInfo test
DECLARE
BEGIN
    orders_utilities.updateProductInfo(17, 'The A-train', 'train products', 500);
END;
/

--removeProduct test
DECLARE
BEGIN
    orders_utilities.removeProduct(1);
END;
/

--addOrder test
DECLARE
BEGIN
    orders_utilities.addOrder(10, 1, 8, 3);
    
    EXCEPTION
        WHEN orders_utilities.not_enough_stock THEN
        DBMS_OUTPUT.PUT_LINE('Bought quantity exceeds the amount available');
END;
/

--cancelOrder test
DECLARE
    orderToDelete   NUMBER;
    product_order   NUMBER;
    amount          NUMBER;
BEGIN
    orderToDelete := 23;
    
    SELECT product_id INTO product_order
    FROM orders
    WHERE order_id = orderToDelete;
    
    SELECT quantity INTO amount
    FROM order_info
    WHERE order_id = orderToDelete;

    orders_utilities.cancelOrder(orderToDelete, product_order, amount);
END;
/

--addReview test
DECLARE
    existing_order orders_type;
BEGIN
    existing_order := orders_type(21, 11, 15, 11, null);
    orders_utilities.addReview(existing_order, 5, 'what a great purchase Alec made!');
END;
/

--updateReview test
DECLARE
BEGIN
    orders_utilities.updateReview(13, 1, 'whatever this is sucks!');
END;
/

--removeReview test
DECLARE
BEGIN
    orders_utilities.removeReview(21);
END;
/


  ---- testing functions ----

  
--getAllProducts test
DECLARE
    cur SYS_REFCURSOR;
    prod_id products.product_id%TYPE;
    prod_name products.name%TYPE;
    prod_category products.category%TYPE;
    p_price products.price%TYPE;
BEGIN
    cur := orders_utilities.getAllProducts;
    LOOP
        FETCH cur INTO prod_id, prod_name, prod_category, p_price;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('product id: ' || prod_id || ', name: ' || prod_name || ', category: ' || prod_category || ', price: ' || p_price);
    END LOOP;
    CLOSE cur;
END;
/

--getProductsByCategory test
DECLARE
    cur SYS_REFCURSOR;
    prod_id products.product_id%TYPE;
    prod_name products.name%TYPE;
    prod_category products.category%TYPE;
    p_price products.price%TYPE;
BEGIN
    cur := orders_utilities.getProductsByCategory('Vehicle');
    LOOP
        FETCH cur INTO prod_id, prod_name, prod_category, p_price;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('product id: ' || prod_id || ', name: ' || prod_name || ', category: ' || prod_category || ', price: ' || p_price);
    END LOOP;
    CLOSE cur;
END;
/

--getWarehouseProductQuantities test
DECLARE
    cur SYS_REFCURSOR;
    wName warehouse.name%TYPE;
    prod_name products.name%TYPE;
    quantity warehouse.quantity%TYPE;
BEGIN
    cur := orders_utilities.getWarehouseProductQuantities;
    LOOP
        FETCH cur INTO wName, prod_name, quantity;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Warehouse Name: ' || wName || ', Product Name: ' || prod_name || ', Quantity: ' || quantity);
    END LOOP;
    CLOSE cur;
END;
/

--getAllReviews test
DECLARE
    cur SYS_REFCURSOR;
    reviewId    reviews.review_id%TYPE;
    score       reviews.score%TYPE;
    flag        reviews.flag%TYPE;
    descr        reviews.description%TYPE;
BEGIN
    cur := orders_utilities.getAllReviews;
    LOOP
        FETCH cur INTO reviewId, score, flag, descr;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Review-id: ' || reviewId || ', Score: ' || score || ', Flag: ' || flag || ', Description: ' || descr);
    END LOOP;
    CLOSE cur;
END;

--getAVGReviewsFor test
DECLARE
    avgReview reviews.score%TYPE;
BEGIN
    avgReview := orders_utilities.getAVGReviewsFor(1);
    DBMS_OUTPUT.PUT_LINE(avgReview);
END;
/

--getAllOrders test
DECLARE
    cur         SYS_REFCURSOR;
    orderId     order_info.order_id%TYPE;
    firstName   customers.fname%TYPE;
    lastName    customers.lname%TYPE;
    prodName    products.name%TYPE;
    storeName   stores.name%TYPE;
    reviewId    reviews.review_id%TYPE;
BEGIN
    cur := orders_utilities.getAllOrders;
    LOOP
        FETCH cur INTO orderId, firstName, lastName, prodName, storeName, reviewId;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Order-id: ' || orderId || ', Customer Name: ' || firstName || ' ' || lastName || ', Product: ' || prodName || ', Store: ' || storeName || ', review-id: ' || reviewId);
    END LOOP;
    CLOSE cur;
END;

--getOrdersFromStore test
DECLARE
    cur SYS_REFCURSOR;
    orderId     order_info.order_id%TYPE;
    firstName   customers.fname%TYPE;
    lastName    customers.lname%TYPE;
    prodName    products.name%TYPE;
    storeName   stores.name%TYPE;
    reviewId    reviews.review_id%TYPE;
BEGIN
    cur := orders_utilities.getOrdersFromStore(6);
    LOOP
        FETCH cur INTO orderId, firstName, lastName, prodName, storeName, reviewId;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Order-id: ' || orderId || ', Customer Name: ' || firstName || ' ' || lastName || ', Product: ' || prodName || ', Store: ' || storeName || ', review-id: ' || reviewId);
    END LOOP;
    CLOSE cur;
END;
/

--getOrdersFromCustomer test
DECLARE
    cur SYS_REFCURSOR;
    orderId     order_info.order_id%TYPE;
    firstName   customers.fname%TYPE;
    lastName    customers.lname%TYPE;
    prodName    products.name%TYPE;
    storeName   stores.name%TYPE;
    reviewId    reviews.review_id%TYPE;
BEGIN
    cur := orders_utilities.getOrdersFromCustomer(1);
    LOOP
        FETCH cur INTO orderId, firstName, lastName, prodName, storeName, reviewId;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Order-id: ' || orderId || ', Customer Name: ' || firstName || ' ' || lastName || ', Product: ' || prodName || ', Store: ' || storeName || ', review-id: ' || reviewId);
    END LOOP;
    CLOSE cur;
END;
/

--getOrdersMadeOn test
DECLARE
    cur SYS_REFCURSOR;
    orderId     order_info.order_id%TYPE;
    firstName   customers.fname%TYPE;
    lastName    customers.lname%TYPE;
    prodName    products.name%TYPE;
    storeName   stores.name%TYPE;
    reviewId    reviews.review_id%TYPE;
BEGIN
    cur := orders_utilities.getOrdersMadeOn('19-DEC-21');
    LOOP
        FETCH cur INTO orderId, firstName, lastName, prodName, storeName, reviewId;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Order-id: ' || orderId || ', Customer Name: ' || firstName || ' ' || lastName || ', Product: ' || prodName || ', Store: ' || storeName || ', review-id: ' || reviewId);
    END LOOP;
    CLOSE cur;
    
    EXCEPTION
        WHEN orders_utilities.is_inexistent THEN
        DBMS_OUTPUT.PUT_LINE('No orders were made on this date');
END;
/

--getAllLogs test
DECLARE
    cur         SYS_REFCURSOR;
    logId       log_table.log_id%TYPE;
    userChange  log_table.user_change%TYPE;
    tableName   log_table.table_name%TYPE;
    action      log_table.action%TYPE;
    logDate     log_table.log_date%TYPE;
BEGIN
    cur := orders_utilities.getAllLogs;
    
    LOOP
        FETCH cur INTO logId, userChange, tableName, action, logDate;
        EXIT WHEN cur%NOTFOUND;
        
        DBMS_OUTPUT.PUT_LINE('Log-id: ' || logId || ', User: ' || userChange || ', Table:' || tableName || ', Action: ' || action || ', Date: ' || logDate);
    END LOOP;
    CLOSE cur;
END;
/
