/**
 * A class for creating an SQL connection to the pdbora database
 */
package utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CreateConnection {
    private String userName;
    private String userPWD;
    private Connection conn;

    
    public CreateConnection(String userName,String userPWD){
        this.userName = userName;
        this.userPWD = userPWD;
    
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try {
            this.conn =
            DriverManager.getConnection(url, this.userName, this.userPWD);
        } catch (SQLException e) {
            System.err.println("invalid user name or password");
            System.exit(0);
        }
    }
    public Connection getConn() {
        return conn;
    }

    public CreateConnection(Connection conn){
        this.conn = conn;
    }


    //closes the connection
    public void close(){
        try {
            if (!this.conn.isClosed()) {
                this.conn.close();
            } 
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
