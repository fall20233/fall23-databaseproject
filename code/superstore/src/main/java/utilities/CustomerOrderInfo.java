/**
 * A class for holding all necessary information a user would want to see on an order
 *      - order id
 *      - customer name
 *      - product name
 *      - store name
 *      - review id
 */

package utilities;
public class CustomerOrderInfo {
    private int orderId;
    private String customerName;
    private String productName;
    private String storeName;
    private int reviewId;

    public CustomerOrderInfo(int orderId, String customerFName,String customerLName, String productName, String storeName, int reviewId) {
        this.orderId = orderId;
        this.customerName = customerFName + " " +  customerLName;
        this.productName = productName;
        this.storeName = storeName;
        this.reviewId = reviewId;
    }
    public CustomerOrderInfo() {}


    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getStoreName() {
        return storeName;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    public int getReviewId() {
        return reviewId;
    }
    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public String toString() {

        String returnString = String.format("%-15d %-30s %-30s %-25s %2d \n",this.orderId,this.customerName,this.productName,this.storeName,this.reviewId);

        return returnString;
    }
    
}
