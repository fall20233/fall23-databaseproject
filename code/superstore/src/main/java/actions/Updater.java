/**
 * a class with methods for updating information from the database
 */
package actions;

import java.sql.*;

public class Updater {
    public Updater(){}

    /**
     * updates a review in the database
     * @param conn an sql connection object to connect to the database
     * @param reviewId the id of the review to update
     * @param newScore a new score for the review
     * @param newDesc a new description for the review
     * @param newFlag a new flag for the review
     * @throws SQLException
     */
    public void updateReview(Connection conn, int reviewId,int newScore,String newDesc,int newFlag) throws SQLException{
        String SQL = "{call orders_utilities.updateReview(?,?,?,?)}";
        CallableStatement statement = conn.prepareCall(SQL);
        statement.setInt(1, reviewId);
        statement.setInt(2, newScore);
        statement.setString(3,newDesc);
        statement.setInt(4, newFlag);
        statement.execute();
    }

    /**
     * updates a warehouses quantity
     * @param conn an sql connection object to connect to the database
     * @param productID the id of the product whose quantity to change
     * @param quantity the amount to change by
     * @param action add or remove from the quantity (0 to add 1 to remove)
     * @throws SQLException
     */
    public void updateWarehouseQuantity(Connection conn, int productID, int quantity, int action) throws SQLException{
        String SQL = "{call orders_utilities.updateWarehouseQuantity(?,?,?)}";
        CallableStatement statement = conn.prepareCall(SQL);
        statement.setInt(1, productID);
        statement.setInt(2, quantity);
        statement.setInt(3, action);
        statement.execute();
    }

    /**
     * updates the information of a product
     * @param conn an sql connection object to connect to the database
     * @param id the id of the product to update
     * @param newName a new name for the product
     * @param newCategory a new category for the product
     * @param newPrice a new price for the product
     * @throws SQLException
     */
    public void updateProductInfo(Connection conn, int id, String newName, String newCategory, double newPrice) throws SQLException{
        String SQL = "{call orders_utilities.updateProductInfo(?,?,?,?)}";
        CallableStatement statement = conn.prepareCall(SQL);
        statement.setInt(1,id);
        statement.setString(2,newName);
        statement.setString(3,newCategory);
        statement.setDouble(4, newPrice);
        statement.execute();
    }
}
