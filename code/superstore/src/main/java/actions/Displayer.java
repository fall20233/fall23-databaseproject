/**
 * a class with methods for viewing information from the database
 */

package actions;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;
import tables.*;
import utilities.Inputs;
import utilities.CustomerOrderInfo;

public class Displayer {
        public static Inputs input = new Inputs();
        
        public Displayer(){}

        /**
         * this method gets all the orders in the database
         * @param conn an sql connection object to connect to the database
         * @return a list of customer order information
         * @throws SQLException
         */
        public List<CustomerOrderInfo> getAllOrders(Connection conn) throws SQLException{
            CustomerOrderInfo COI = new CustomerOrderInfo();
            List<CustomerOrderInfo> orderInfos = new ArrayList<>();
            String sql = "{? = call orders_utilities.getAllOrders}";
            CallableStatement statement = conn.prepareCall(sql);
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.executeQuery();
            ResultSet rs = (ResultSet)statement.getObject(1);

            while(rs.next()){
                COI = new CustomerOrderInfo(
                    rs.getInt("orderId"),
                    rs.getString("FirstName"),
                    rs.getString("LastName"),
                    rs.getString("ProductName"),
                    rs.getString("StoreName"),
                    rs.getInt("ReviewId")
                );
                orderInfos.add(COI);
            }
            return orderInfos;

        }

        /**
         * this method gets a specific order based on the input order id
         * @param conn an sql connection object to connect to the database
         * @param orderId the id of an order in the database
         * @return an order object
         * @throws SQLException
         */
        public Orders getOrder(Connection conn,int orderId) throws SQLException{
            Orders order = new Orders();
            String sql = "{? = call orders_utilities.getOrder(?)}";
            CallableStatement statement = conn.prepareCall(sql);
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.setInt(2, orderId);
            statement.executeQuery();
            ResultSet rs = (ResultSet)statement.getObject(1);

            while(rs.next()){
                order = new Orders(
                    rs.getInt("OrderId"),
                    rs.getInt("CustomerID"),
                    rs.getInt("ProductId"),
                    rs.getInt("StoreId"),
                    rs.getInt("ReviewId")
                );
            }
            return order;
            
        }

    /**
     * Prompts the user to enter information for a date
     * @return an sql date object (year-mm-dd)
     */
    public Date getADate(){
        String date = "";
        System.out.print("enter a full year (eg: 2000): ");
        String year = input.getUserString();
        date += year + "-";

        System.out.print("enter a month (in numbers eg january = 1): ");
        String month = input.getUserString();
        date += month + "-";

        System.out.print("enter a day: ");
        String day = input.getUserString();
        date += day;

        return Date.valueOf(date);
    }

    /**
     * this method gets all the product categories in the database
     * @param conn an sql connection object to connect to the database
     * @return a string list of categories
     * @throws SQLException
     */
    public List<String> getCategories(Connection conn) throws SQLException{
        List<String> categories = new ArrayList<>();
        String categorie;
        String sql = "{? = call orders_utilities.getCategories}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);
        while (rs.next()){
            categorie = rs.getString("category");
            categories.add(categorie);
        }
        return categories;

    }

    /**
     * this method gets all products with a specific category
     * @param conn an sql connection object to connect to the database
     * @param category a string representing a category in the database
     * @return a list of product objects
     * @throws SQLException
     */
    public List<Products> getProductsByCategory(Connection conn,String category) throws SQLException{
        List<Products> products = new ArrayList<>();
        Products prod = new Products();
        String sql = "{? = call orders_utilities.getProductsByCategory(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.setString(2, category);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);
        while (rs.next()){
            
            prod = new Products(
                rs.getInt("product_id"),
                rs.getString("name"),
                rs.getString("category"),
                rs.getDouble("price")
            );
            products.add(prod);
        }
        return products;
    }

    /**
     * this method gets all the products in the database
     * @param conn an sql connection object to connect to the database
     * @return a list of product objects
     * @throws SQLException
     */
    public List<Products> getAllProducts(Connection conn) throws SQLException{
        List<Products> products = new ArrayList<>();
        Products prod = new Products();
        String sql = "{? = call orders_utilities.getAllProducts}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);
        while (rs.next()){
            prod = new Products(
                rs.getInt("product_id"),
                rs.getString("name"),
                rs.getString("category"),
                rs.getDouble("price")
            );
            products.add(prod);
        }

        return products;
    }

    /**
     * this method gets the amount of every product in the warehouses
     * @param conn an sql connection object to connect to the database
     * @return a formatted string of products
     * @throws SQLException
     */
    public String getProductsQtyInWarehouses(Connection conn) throws SQLException{
        String sql = "{? = call orders_utilities.getWarehouseProductQuantities}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        String returnString = String.format("\n %-20s %-20s %-2s \n","Warehouse name","product","quantity");
        returnString += String.format("%-50s \n"," ").replace(" ", "-");
        while(rs.next()){
            returnString += String.format("%-20s %-20s %-2d \n",rs.getString("wName"),rs.getString("pName"),rs.getInt("pQty"));
        }

        return returnString;
    }

    /**
     * this method takes in a list of products and formats them into a string
     * @param products a list of product objects
     * @return a formatted string of products
     */
    public String showProducts(List<Products> products){
        String returnString = String.format("\n %-20s %-20s %-20s %-2s \n","product id","product name","category","price");
        returnString += String.format("%-70s \n"," ").replace(" ", "-");
        for(Products prod : products){
            returnString += String.format("%-20d %-20s %-20s %-2.2f$ \n", prod.getProductID(),prod.getName(),prod.getCategory(),prod.getPrice());
        }
        return returnString;
    }

    /**
     * turns a list of customerOrderInfo objects into a formatted string
     * @param orderInfos a list of customer order info objects
     * @return a formatted string
     */
    public String showCustomerOrders(List<CustomerOrderInfo> orderInfos){
        String returnString = String.format("%-15s %-30s %-30s %-20s %s \n", "Order id","Customer name","product name","Store name", "review id");

        returnString += String.format("%110s \n"," ").replace(" ", "-");

        for (CustomerOrderInfo COI : orderInfos) {
            returnString += COI.toString();
        }
        return returnString;
    }

    /**
     * A method to get all the stores in the database
     * @param conn an sql connection object to connect to the database
     * @return a formatted string of stores
     * @throws SQLException
     */
    public String seeAllStores(Connection conn) throws SQLException {
        String returnString = "";
        String sql = "{? = call orders_utilities.getStoreIds}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        returnString = (String.format("%-20s %-2s \n","Store name","Store id"));
        returnString += (String.format("%-35s \n"," ").replace(" ", "-"));
        while (rs.next()){
            returnString += String.format("%-20s %-2d \n", rs.getString("storeName"),rs.getInt("storeId"));
        }

        return returnString;
    }

    /**
     * this method gets all the orders in the database made at the inputted store
     * @param conn an sql connection object to connect to the database
     * @param storeId the id of a store in the database
     * @return a formatted string of orders
     * @throws SQLException
     */
    public String getOrdersByStore(Connection conn, int storeId) throws SQLException{
        String returnString = "";
        returnString += String.format("\n %-10s %-20s %-20s %-20s %-20s %-10s \n","Order Id","First Name","Last Name","Product name","Store Name", "Review id");
        returnString += (String.format("%105s \n"," ").replace(" ", "-"));

        String sql = "{? = call orders_utilities.getOrdersFromStore(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.setInt(2,storeId);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        while(rs.next()){
            returnString += String.format("%-10d %-20s %-20s %-20s %-20s %-10d \n",rs.getInt("OrderId"),rs.getString("FirstName"),rs.getString("LastName"),rs.getString("ProductName"),rs.getString("StoreName"), rs.getInt("ReviewId"));
        }
        return returnString;
    }

    /**
     * a method to get the audit log in the database
     * @param conn an sql connection object to connect to the database
     * @return a formatted string of the audit log
     * @throws SQLException
     */
    public String getLogs(Connection conn) throws SQLException{
        String returnString = String.format("\n%-10s %-15s %-15s %-10s %-10s \n","Log Id","User","Table Name","Action ","Log Date");
        returnString += (String.format("%64s \n"," ").replace(" ", "-"));

        String sql = "{? = call orders_utilities.getAllLogs}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        while (rs.next()) {
            returnString += String.format("%-10d %-15s %-15s %-10s %-10s \n",rs.getInt("LogId"),rs.getString("UserChange"),rs.getString("TableName"),rs.getString("Action"),rs.getDate("LogDate").toString());
        }
        return returnString;
    }

    /**
     * this method gets all orders in the database made by a customer
     * @param conn an sql connection object to connect to the database
     * @param CustomerId the id of the customer
     * @return a formatted string of the orders
     * @throws SQLException
     */
    public String getOrdersFromCustomer(Connection conn,int CustomerId) throws SQLException{
        String returnString = String.format("\n%-10s %-20s %-20s %-20s %-20s %s\n", "Order id","First Name","Last Name","Product Name", "Store Name","Review id");
        returnString += (String.format("%105s \n"," ").replace(" ", "-"));

        String sql = "{? = call orders_utilities.getOrdersFromCustomer(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.setInt(2, CustomerId);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        while(rs.next()){
            returnString += String.format("%-10d %-20s %-20s %-20s %-20s %d\n", rs.getInt("Orderid"),rs.getString("FirstName"),rs.getString("LastName"),rs.getString("ProductName"), rs.getString("StoreName"),rs.getInt("ReviewId"));
        }

        return returnString;
    }

    /**
     * this method gets all orders made on a specific day
     * @param userDate a date object representing the day to look at
     * @param conn an sql connection object to connect to the database
     * @return a formatted string of orders
     * @throws SQLException
     */
    public String getOrdersMadeOn(Date userDate, Connection conn) throws SQLException{
        String returnString = String.format("\n%-10s %-20s %-20s %-20s %-20s %s\n", "Order id","First Name","Last Name","Product Name", "Store Name","Review id");
        returnString += (String.format("%105s \n"," ").replace(" ", "-"));

        String sql = "{? = call orders_utilities.getOrdersMadeOn(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.setDate(2, userDate);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);

        while(rs.next()){
            returnString += String.format("%-10d %-20s %-20s %-20s %-20s %d\n", rs.getInt("Orderid"),rs.getString("FirstName"),rs.getString("LastName"),rs.getString("ProductName"), rs.getString("StoreName"),rs.getInt("ReviewId"));
        }

        return returnString;
    }

    /**
     * this method gets the average reviews for a product
     * @param prodId the product to look at
     * @param conn an sql connection object to connect to the database
     * @return a string with the average review score
     * @throws SQLException
     */
    public String getAVGReviewsFor(int prodId,Connection conn) throws SQLException{
        String returnString = "Average score: ";
        Double avg = -100.0;
        String sql = "{? = call orders_utilities.getAVGReviewsFor(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, Types.DOUBLE);
        statement.setInt(2,prodId);
        statement.executeQuery();
        avg = (Double)statement.getObject(1);

        returnString += avg;

        return returnString;
    }

    /**
     * gets all the reviews that have been flagged
     * @param conn an sql connection object to connect to the database
     * @return a formatted string of flagged reviews
     * @throws SQLException
     */
    public String getFlagged(Connection conn) throws SQLException{
        String returnString = String.format("\n%-10s %-10s %-10s %-30s\n","Review id","score","flag","description");
        returnString += (String.format("%45s \n"," ").replace(" ", "-"));
        String sql = "{? = call orders_utilities.getFlaggedReviews}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.registerOutParameter(1, OracleTypes.CURSOR);
        statement.executeQuery();
        ResultSet rs = (ResultSet)statement.getObject(1);
        while(rs.next()){
            returnString += String.format("%-10d %-10d %-10d %-30s\n",rs.getInt("ReviewID"),rs.getInt("score"),rs.getInt("flag"),rs.getString("description"));
        }
        return returnString;
    }
}
