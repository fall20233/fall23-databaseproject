package tables;
import utilities.Validator;
import java.sql.*;

public class Provinces{
    
    public static final String type_name = "PROVINCE_TYPE";
    private int province_id;
    private String province_name;
    private String country_name;


    public Provinces(int province_id, String province_name, String country_name) {
        Validator check = new Validator();
        check.validateInt(province_id, 0, 99);
        check.validateString(province_name, 20);
        check.validateString(country_name, 25);

        this.province_id = province_id;
        this.province_name = province_name;
        this.country_name = country_name;
    }
    
    public int getProvince_id() {
        return this.province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public String getProvince_name() {
        return this.province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getCountry_name() {
        return this.country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }
    
    
    public void addToDatabase(Connection conn) throws ClassNotFoundException{
        throw new UnsupportedOperationException("unimplemented method 'add to database");
    //try {
    //     Map map = conn.getTypeMap();
    //     conn.setTypeMap(map);
    //     map.put("PROVINCE_TYPE", Class.forName("database2.Provinces"));
    //     String sql = "{call add_province(?)}";
    //         try(CallableStatement statement = conn.prepareCall(sql)){
        //             statement.setObject(1,this);
    //             statement.execute();
    //         }
    //     } catch (SQLException e) {
    //         e.printStackTrace();
    //     }
    
}   
    @Override
    public String toString() {
        return "Provinces [province_id=" + province_id + ", province_name=" + province_name + ", country_name="
                + country_name + "]";
    }
}
