package tables;

import java.sql.*;
import utilities.Validator;

public class OrderInfo implements SQLData{

    public final String type_name = "ORDER_INFO_TYPE";
    private int order_id;
    private Date order_date;
    private int quantity;

    
    public OrderInfo() {}
    
    public OrderInfo(int order_id, Date order_date,int quantity){
        Validator check = new Validator();
        check.validateInt(order_id, 0, 99);
        check.validateInt(quantity, 0, 999);

        this.order_id = order_id;
        this.order_date = order_date;
        this.quantity = quantity;
    }

    public int getOrder_id() {
        return order_id;
    }
    
    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public Date getOrder_date() {
        return order_date;
    }
    
    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    //overrides
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.type_name;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setOrder_id(stream.readInt());
        setOrder_date(stream.readDate());  
        setQuantity(stream.readInt()); 
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getOrder_id());
        stream.writeDate(getOrder_date());
        stream.writeInt(getQuantity());
    }

    @Override
    public String toString() {
        return "OrderInfo [order_id=" + this.order_id + ", order_date=" + this.order_date + ", quantity=" + this.quantity + "]";
    }
    
}
