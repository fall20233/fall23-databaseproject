package tables;

import utilities.Validator;

public class Customers{
    
    public static final String type_name = "CUSTOMERS_TYPE";
    private int customer_id;
    private String fname;
    private String lname;
    private String email;
    
    public Customers(int customer_id, String fname, String lname, String email) {
        Validator check = new Validator();
        check.validateInt(customer_id, 0, 99);
        check.validateString(fname, 30);
        check.validateString(lname, 30);
        check.validateString(email, 30);

        this.customer_id = customer_id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString() {
        return "Customers [customer_id=" + customer_id + ", fname=" + fname + ", lname=" + lname + ", email=" + email                            + "]";
    }
    
}
