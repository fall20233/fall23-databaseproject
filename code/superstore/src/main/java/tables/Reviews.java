package tables;

import java.sql.*;
import java.util.Map;

import utilities.Validator;

public class Reviews implements SQLData{

    public final String type_name = "REVIEWS_TYPE";
    private int review_id;
    private int score;
    private int flag;
    private String description;
    
    public Reviews(int review_id, int score, int flag, String description) {
        Validator check = new Validator();
        check.validateInt(review_id, 0, 99);
        check.validateInt(score, 0, 5);
        check.validateInt(flag, 0, 10);

        this.review_id = review_id;
        this.score = score;
        this.flag = flag;
        this.description = description;
    }

    public Reviews() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReview_id() {
        return review_id;
    }
    
    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }

    public int getScore() {
        return score;
    }
    
    public void setScore(int score) {
        this.score = score;
    }
    
    public int getFlag() {
        return flag;
    }
    
    public void setFlag(int flag) {
        this.flag = flag;
        
    }

    public void addToDatabase(Connection conn,Orders existingOrder) throws ClassNotFoundException{
    
        try {
                Map map = conn.getTypeMap();
                conn.setTypeMap(map);
                map.put("REVIEWS_TYPE", Class.forName("tables.Reviews"));
                String sql = "{call orders_utilities.addReview(?,?,?)}";
                    try(CallableStatement statement = conn.prepareCall(sql)){
                            statement.setObject(1,existingOrder);
                            statement.setInt(2,this.score);
                            statement.setString(3,this.description);
                            statement.execute();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }         
    }

    //overrides
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.type_name;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setReview_id(stream.readInt());
        setScore(stream.readInt());
        setFlag(stream.readInt());
        setDescription(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
       stream.writeInt(getReview_id());
       stream.writeInt(getScore());
       stream.writeInt(getFlag());
       stream.writeString(getDescription());

    }
    @Override
    public String toString() {
        return "Reviews [review_id=" + review_id + ", score=" + score + ", flag=" + flag + ", description="
                + description + "]";
    }
}
