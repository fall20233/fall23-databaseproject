package tables;

import utilities.Validator;

public class WarehouseInfo{

    public final String Warehouse_info_type = "WAREHOUSE_INFO_TYPE";
    private String name;
    private String warehouse_address;
    private String city;

    public WarehouseInfo(String name, String warehouse_address, String city){
        Validator check = new Validator();
        check.validateString(name, 30);
        check.validateString(warehouse_address, 30);
        check.validateString(city, 25);

        this.name = name;
        this.warehouse_address = warehouse_address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWarehouse_address() {
        return warehouse_address;
    }

    public void setWarehouse_address(String warehouse_address) {
        this.warehouse_address = warehouse_address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "WarehouseInfo [name=" + name + ", warehouse_address=" + warehouse_address + ", city=" + city + "]";
    }
    
}
