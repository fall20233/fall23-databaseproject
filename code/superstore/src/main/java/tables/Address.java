package tables;
import utilities.Validator;

public class Address{
    public static final String type_name = "ADDRESS_TYPE";
    private int customer_id;
    private String address;
    private String city;
    
    public Address(int customer_id, String address, String city) {
        Validator check = new Validator();
        check.validateInt(customer_id, 0, 99);
        check.validateString(address, 50);
        check.validateString(city, 25);

        this.customer_id = customer_id;
        this.address = address;
        this.city = city;
    }
    
    public int getCustomer_id() {
        return customer_id;
    }
    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
                
    @Override
    public String toString() {
        return "address [customer_id=" + customer_id + ", address=" + address + ", city=" + city + "]";
    }
}
