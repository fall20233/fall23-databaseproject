package tables;

import utilities.Validator;

public class Location{
    
    public static final String type_name = "LOCATION_TYPE";
    private String city;
    private int province_id;
    
    public Location(String city, int province_id) {
        Validator check = new Validator();
        check.validateString(city, 25);
        check.validateInt(province_id, 0, 99);
        this.city = city;
        this.province_id = province_id;
    }
    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    public int getProvince_id() {
        return province_id;
    }
    
    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }


    @Override
    public String toString() {
        return "Location [city=" + city + ", province_id=" + province_id + "]";
    }
}
