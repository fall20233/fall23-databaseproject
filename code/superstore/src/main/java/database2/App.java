/**
 * Main application class for interacting with the database
 */
package database2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import utilities.*;
import actions.*;
import tables.*;

public class App {
    private static Inputs input = new Inputs();
    private static Displayer view = new Displayer();
    private static Updater update = new Updater();
    private static Deleter delete = new Deleter();
    private static Connection conn;

    public static void main(String[] args) throws SQLException {
        CreateConnection connection = getConnection();
        conn = connection.getConn();

        try {
            userActions();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
            input.close();
        }

    }

    /**
     * creates an sql connection
     * 
     * @return
     */
    public static CreateConnection getConnection() {
        System.out.print("Enter your username: ");
        String userName = input.getUserString();
        System.out.print("Enter your password: ");
        String userPWD = input.getUserString();

        CreateConnection connection = new CreateConnection(userName, userPWD);
        return connection;
    }

    /**
     * prompts the user to choose an action and calls a method to do that action
     * 
     * @throws SQLException
     */
    public static void userActions() throws SQLException {
        List<String> actions = new ArrayList<>();
        actions.add("end"); // 1
        actions.add("Display"); // 2
        actions.add("add"); // 3
        actions.add("delete"); // 4
        actions.add("update"); // 5

        System.out.println("Choose an action: ");
        int curr = 1;
        for (String string : actions) {
            System.out.println(curr + " action: " + string);
            curr++;
        }

        int action = -1;
        try {
            action = input.getUserInteger();
            if (action < 1 || action > 5) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException NFE) {
            System.out.println("Not a valid number");
            userActions();
        }

        performAction(action);
    }

    /**
     * takes the users choice of action and calls the appropriate perform method
     * 
     * @param action a number representing the action chosen by the user
     * @throws SQLException
     */
    public static void performAction(int action) throws SQLException {
        switch (action) {
            case 1:
                System.out.println("bye-bye :)");
                System.exit(0);
            case 2:
                performDisplayAction();
                break;
            case 3:
                performAddAction();
                break;
            case 4:
                performDeleteAction();
                break;
            case 5:
                performUpdateAction();
                break;
            default:
                System.out.println("invalid action number");
                break;
        }
        System.out.println();
        userActions();
    }

    /**
     * prompts the user to choose what information they want to view
     * 
     * @throws SQLException
     */
    public static void performDisplayAction() throws SQLException {
        List<String> actions = new ArrayList<>();
        actions.add("All products"); // 1
        actions.add("All products in a category"); // 2
        actions.add("Amount of products in all the warehouses"); // 3
        actions.add("See all orders"); // 4
        actions.add("Orders of a local store"); // 5
        actions.add("A customer's orders"); // 6
        actions.add("All orders on a certain day"); // 7
        actions.add("The Audit log"); // 8
        actions.add("Average review score for a product"); // 9
        actions.add("flagged reviews"); // 10
        actions.add("cancel"); // 11

        System.out.println("what would you like to view?");
        int curr = 1;
        for (String string : actions) {
            System.out.println(curr + " view: " + string);
            curr++;
        }
        int action = -1;
        try {
            action = input.getUserInteger();
        } catch (NumberFormatException e) {
            System.out.println("not a valid number");
            performDisplayAction();
        }

        switch (action) {
            case 1:
                // shows all products
                System.out.println(view.showProducts(view.getAllProducts(conn)));
                break;
            case 2:
                // shows products based on a category the user chose
                String category = getUserCategory();
                System.out.println(view.showProducts(view.getProductsByCategory(conn, category)));
                break;
            case 3:
                // get the amount of each product in the warehouses
                System.out.println(view.getProductsQtyInWarehouses(conn));
                break;
            case 4:
                // shows all orders
                System.out.println(view.showCustomerOrders(view.getAllOrders(conn)));
                break;
            case 5:
                seeOrdersForStore();
                break;
            case 6:
                getACustomersOrders();
                break;
            case 7:
                getOrdersOnDate();
                break;
            case 8:
                getAllLogs();
                break;
            case 9:
                getAvgReviewForProduct();
                break;
            case 10:
                changeFlaggedReview();
                break;
            case 11:
                return;
            default:
                System.out.println("invalid action");
                performDisplayAction();
        }
        // waits for the user to be done viewing and then returns to userActions method
        System.out.println("<press Enter to continue>");
        input.getUserString();
        userActions();
    }

    /**
     * prints all product categories and prompts the user to choose one
     * 
     * @return
     * @throws SQLException
     */
    private static String getUserCategory() throws SQLException {
        String userCat = "";
        List<String> categories = view.getCategories(conn);
        System.out.println("categories");
        for (String category : categories) {
            System.out.println(category);
        }

        System.out.println();
        System.out.println("choose a category");
        do {
            userCat = input.getUserString();
        } while (!categories.contains(userCat));

        return userCat;
    }

    /**
     * prompts the user for a product's id and prints the average reviews for that
     * product
     */
    private static void getAvgReviewForProduct() {
        try {
            System.out.print("Enter the product's id: ");
            int prodID = input.getUserInteger();
            System.out.println(view.getAVGReviewsFor(prodID, conn));
        } catch (NumberFormatException NFE) {
            System.out.println("Not a number");
            getAvgReviewForProduct();
        } catch (SQLException e) {
            System.out.println("invalid product id");
            getAvgReviewForProduct();
        }
    }

    /**
     * gets a date object from the user and prints all orders made on that day
     */
    private static void getOrdersOnDate() {
        Date userDate = view.getADate();
        try {
            System.out.println(view.getOrdersMadeOn(userDate, conn));
        } catch (SQLException e) {
            System.out.println("no orders made on that day");
            getOrdersOnDate();
        }
    }

    /**
     * asks the user for a customer's id and prints all orders from that customer
     */
    private static void getACustomersOrders() {
        try {
            System.out.println(view.getOrdersFromCustomer(conn, getUserCustomerId()));
        } catch (SQLException e) {
            System.out.println("Invalid customer id");
            getACustomersOrders();
        }
    }

    /**
     * gets a customer id from the user
     * 
     * @return the inputted customer id
     */
    private static int getUserCustomerId() {
        int UserCustomerId = -1;
        try {
            System.out.print("Enter the customer's id: ");
            UserCustomerId = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            getUserCustomerId();
        }
        return UserCustomerId;
    }

    /**
     * prints the audit log
     * 
     * @throws SQLException
     */
    private static void getAllLogs() throws SQLException {
        System.out.println(view.getLogs(conn));
    }

    /**
     * asks the users if they want to view all stores
     * prompts the user to input a store id and prints all orders made at that store
     * 
     * @throws SQLException
     */
    private static void seeOrdersForStore() throws SQLException {
        List<String> options = new ArrayList<>();
        options.add("See all stores and their ids");
        options.add("Enter a store id to view");
        options.add("cancel");
        System.out.println("Choose an action: ");
        int curr = 1;
        for (String opt : options) {
            System.out.println(curr + " action: " + opt);
            curr++;
        }
        int userOption = -1;
        try {
            userOption = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("Invalid action \n");
            seeOrdersForStore();
        }
        switch (userOption) {
            case 1:
                System.out.println(view.seeAllStores(conn));
                userOption = 2;
            case 2:
                // gets the id of a store from the user
                int userStoreId = getUserStoreId();
                try {
                    System.out.println(view.getOrdersByStore(conn, userStoreId));
                } catch (SQLException e) {
                    System.out.println("Store does not exist");
                }

                break;
            case 3:
                return;
            default:
                System.out.println("Invalid number input");
                seeOrdersForStore();
        }

    }

    /**
     * prompts the user for the id of a store
     * 
     * @return
     */
    private static int getUserStoreId() {
        int userStoreId = -1;
        try {
            System.out.print("Enter a store id: ");
            userStoreId = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            getUserStoreId();
        }
        return userStoreId;
    }

    /**
     * prints all reviews that have been flagged and prompts the user to do update
     * or delete it
     * 
     * @throws SQLException
     */
    private static void changeFlaggedReview() throws SQLException {
        System.out.println(view.getFlagged(conn));

        List<String> actionsOnFlag = new ArrayList<>();
        actionsOnFlag.add("update review");
        actionsOnFlag.add("delete review");
        actionsOnFlag.add("nothing");

        System.out.println("What do you want to do with these reviews?");
        int curr = 1;
        for (String action : actionsOnFlag) {
            System.out.println(curr + " " + action);
            curr++;
        }
        int userAction = -1;
        try {
            userAction = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            changeFlaggedReview();
        }

        // calls the appropriate function based on the chosen action or returns
        switch (userAction) {
            case 1:
                updateReview();
                break;
            case 2:
                deleteReview();
                break;
            case 3:
                return;
            default:
                System.out.println("not a valid action");
                changeFlaggedReview();
        }
    }

    /**
     * handles adding an entry to the database
     * 
     * @throws SQLException
     */
    public static void performAddAction() throws SQLException {
        List<String> actions = new ArrayList<>();
        actions.add("an order"); // 1
        actions.add("a product"); // 2
        actions.add("add a review to a product"); // 3
        actions.add("cancel"); // 4

        System.out.println("what would you like to add?");
        int curr = 1;
        for (String string : actions) {
            System.out.println(curr + " add: " + string);
            curr++;
        }
        int action = -1;
        try {
            action = input.getUserInteger();

        } catch (NumberFormatException NFE) {
            System.out.println("Not a number");
            performAddAction();
        }
        // call the appropriate method depending on what the user wants to add
        switch (action) {
            case 1:
                addAnOrder();
                break;
            case 2:
                addAProduct();
                break;
            case 3:
                addAReview();
                break;
            case 4:
                return;
            default:
                break;
        }
    }

    /**
     * prompts the user for an order id to add the review too
     * asks the user for the review score and description and adds that review to
     * the database
     */
    private static void addAReview() {
        Orders existingOrder = new Orders();
        Reviews newReview = new Reviews();

        System.out.print("Enter the id of the order you want to add a review to: ");
        try {
            existingOrder = view.getOrder(conn, input.getUserInteger());
        } catch (SQLException e) {
            System.out.println("invalid order id");
            addAReview();
        }

        System.out.print("Enter the review score: ");
        newReview.setScore(input.getUserInteger());
        System.out.print("Enter the review description: ");
        newReview.setDescription(input.getUserString());

        try {
            newReview.addToDatabase(conn, existingOrder);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * prompts the user for information on a product and adds it to the database
     */
    private static void addAProduct() {
        Products newProduct = new Products();

        try {
            System.out.print("Enter the product name: ");
            newProduct.setName(input.getUserString());

            System.out.print("Enter the product's category: ");
            newProduct.setCategory(input.getUserString());

            System.out.print("Enter the product's price: ");
            newProduct.setPrice(input.getUserDouble());

            System.out.print("Enter the name of the warehouse the poduct is from (E.g warehouse A): ");
            String wareName = input.getUserString();
            System.out.print("Enter the quantity of the product in the warehouse: ");
            int wareQty = input.getUserInteger();

            newProduct.addToDatabase(conn, wareName, wareQty);

        } catch (NumberFormatException NFE) {
            System.out.println("invalid number");
            addAProduct();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * prompts the user to input information for a new order and adds it to the database
     */
    private static void addAnOrder() {
        Orders newOrder = new Orders();
        try {
            System.out.print("Enter the id of the customer: ");
            newOrder.setCustomer_id(input.getUserInteger());
            System.out.print("Enter the product's id: ");
            newOrder.setProduct_id(input.getUserInteger());
            System.out.print("Enter the store's id: ");
            newOrder.setStore_id(input.getUserInteger());
            System.out.print("Enter the amount bought: ");
            int quantity = input.getUserInteger();

            newOrder.addToDatabase(conn, quantity);
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            addAnOrder();
        } catch (ClassNotFoundException CNFE) {

        }

    }

    /**
     * Handles the user choosing to update an entry
     * @throws SQLException
     */
    public static void performUpdateAction() throws SQLException {
        List<String> actions = new ArrayList<>();
        actions.add("Update review"); // 1
        actions.add("Update product info"); // 2
        actions.add("Update warehouse quantity"); // 3
        actions.add("cancel"); // 4

        System.out.println("What do you want to update?");
        int curr = 1;
        for (String action : actions) {
            System.out.println(curr + " " + action);
            curr++;
        }
        int action = -1;
        try {
            action = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            performUpdateAction();
        }

        //calls the appropriate function based on the user's choice
        switch (action) {
            case 1:
                updateReview();
                break;
            case 2:
                updateProduct();
                break;
            case 3:
                updateWarehouse();
                break;
            case 4:
                return;
            default:
                System.out.println("Invalid number");
                performUpdateAction();
        }
    }

    /**
     * asks the user for new information on a warehouse and updates that entry
     */
    private static void updateWarehouse() {
        try {
            System.out.print("Enter id of product quantity to change: ");
            int prodID = input.getUserInteger();

            System.out.print("Enter the amount to change by: ");
            int quantity = input.getUserInteger();

            System.out.print("Are you adding or removing (0 to add, 1 to remove): ");
            int action = input.getUserInteger();

            update.updateWarehouseQuantity(conn, prodID, quantity, action);
        } catch (NumberFormatException e) {
            System.out.println("not a number");
            updateWarehouse();
        } catch (SQLException e) {
            System.out.println("invalid product id");
            updateWarehouse();
        }
    }

    /**
     * asks the user for a product id and what to change its information to
     * @throws SQLException
     */
    private static void updateProduct(){
        try {
            System.out.print("Enter id of product to update: ");
            int id = input.getUserInteger();

            System.out.print("Enter a new name for the product: ");
            String newName = input.getUserString();

            System.out.print("Enter a new category for the product: ");
            String newCategory = input.getUserString();

            System.out.print("Enter a new price for the product: ");
            double newPrice = input.getUserDouble();

            update.updateProductInfo(conn, id, newName, newCategory, newPrice);

        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            updateProduct();
        } catch(SQLException e){
            System.out.println("no product with that id");
            updateProduct();
        }
    }

    /**
     * asks the user for a review's id and what to change its information to
     */
    private static void updateReview() {
        try {
            System.out.print("Enter id of the review to update: ");
            int reviewId = input.getUserInteger();

            System.out.print("Enter new score: ");
            int newScore = input.getUserInteger();

            System.out.print("Enter new description: ");
            String newDesc = input.getUserString();

            System.out.print("Enter new review flag: ");
            int newFlag = input.getUserInteger();

            update.updateReview(conn, reviewId, newScore, newDesc, newFlag);
        } catch (NumberFormatException e) {
            System.out.println("not a number");
            updateReview();
        } catch (SQLException e) {
            System.out.println("invalid review id");
            updateReview();
        }
    }

    /**
     * handles the user choosing to delete an entry
     * @throws SQLException
     */
    public static void performDeleteAction() throws SQLException {
        List<String> actions = new ArrayList<>();
        actions.add("Delete an order"); // 1
        actions.add("Delete a review"); // 2
        actions.add("Delete a warehouse"); // 3
        actions.add("cancel"); // 4

        System.out.println("What do you want to delete?");
        int curr = 1;
        for (String action : actions) {
            System.out.println(curr + " " + action);
            curr++;
        }
        int action = -1;
        try {
            action = input.getUserInteger();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            performUpdateAction();
        }

        //calls the appropriate delete function based on the choosing action
        switch (action) {
            case 1:
                deleteOrder();
                break;
            case 2:
                deleteReview();
                break;
            case 3:
                deleteWarehouse();
                break;
            case 4:
                return;
            default:
                System.out.println("Not a valid action");
                performDeleteAction();
                break;
        }
    }

    /**
     * asks the user for the id of the order they want to delete and removes it from the database
     */
    private static void deleteOrder() {
        System.out.print("Enter the id of the order to delete: ");
        try {
            delete.deleteOrder(conn, input.getUserInteger());
        } catch (SQLException e) {
            System.out.println("invalid order id");
            deleteOrder();
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            deleteOrder();
        }
    }

    /**
     * asks the user for a review id and deletes it from the database
     */
    private static void deleteReview() {
        System.out.print("Enter the id of the review to delete: ");
        try {
            delete.deleteReview(conn, input.getUserInteger());
        } catch (NumberFormatException NFE) {
            System.out.println("not a number");
            deleteReview();
        } catch (SQLException e) {
            System.out.println("Invalid review id");
            deleteReview();
        }
    }

    /**
     * asks the user for a warehouse name and deletes it from the database
     */
    private static void deleteWarehouse() {
        System.out.println("Enter the name of the warehouse to delete (eg warehouse A): ");
        try {
            delete.deleteWarehouse(conn, input.getUserString());
        } catch (SQLException e) {
            System.out.println("Invalid warehouse name");
            deleteWarehouse();
        }
    }
}